const { BaseRepository } = require('./baseRepository');

class ChatRepository extends BaseRepository {
    constructor() {
        super('chats');
    }
}

exports.ChatRepository = new ChatRepository();
