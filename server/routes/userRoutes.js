const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {authorizationMiddleware} = require('../middlewares/authorization.middleware');
const {adminMiddleware} = require('../middlewares/admin.middleware');

const router = Router();
router.use(
    authorizationMiddleware,
    adminMiddleware
);
router
    .get('/', (req, res, next) => {
        try {
            req.data = UserService.getAll();
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .get('/:id', (req, res, next) => {
        try {
            req.data = UserService.getById(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .post('/', createUserValid, (req, res, next) => {
        try {
            const {body: userData} = req;
            console.log('CREATE WORK!!!');
            req.data = UserService.create(userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .put('/:id', updateUserValid, (req, res, next) => {
        try {
            const {body: userData, params: {id}} = req;
            req.data = UserService.update(id, userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .delete('/:id', (req, res, next) => {
        try {
            req.data = UserService.delete(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    });

module.exports = router;
