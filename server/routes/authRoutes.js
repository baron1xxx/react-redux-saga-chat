const {Router} = require('express');
const AuthService = require('../services/authService');
const userService = require('../services/userService');
const {authenticationMiddleware} = require('../middlewares/authentication.middleware');
const {authorizationMiddleware} = require('../middlewares/authorization.middleware');

const router = Router();

router
    .get('/user', authorizationMiddleware, (req, res, next) => {
        try {
            const {user: {email}} = req;
            req.data = userService.loadUser(email);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .post('/login', authenticationMiddleware, (req, res, next) => {
        try {
            const {body: userData} = req;
            req.data = AuthService.login(userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    });

module.exports = router;
