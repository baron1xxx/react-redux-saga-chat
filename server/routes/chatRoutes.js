const {Router} = require('express');
const ChatService = require('../services/chatService');
const {createChatValid, updateChatValid} = require('../middlewares/chat.validation.middleware');
const {authorizationMiddleware} = require('../middlewares/authorization.middleware');


const router = Router();
router.use(
    authorizationMiddleware
);
router
    .get('/', (req, res, next) => {
        try {
            req.data = ChatService.getAll();
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .get('/:id', (req, res, next) => {
        try {
            req.data = ChatService.getById(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .post('/', createChatValid, (req, res, next) => {
        try {
            const {body: chatData, user} = req;
            req.data = ChatService.create(chatData, user);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .put('/:id', updateChatValid, (req, res, next) => {
        try {
            const {body: chatData, params: {id}} = req;
            req.data = ChatService.update(id, chatData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .delete('/:id', (req, res, next) => {
        try {
            req.data = ChatService.delete(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    });

module.exports = router;
