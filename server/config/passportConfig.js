const passport = require('passport');
const { Strategy: LocalStrategy } = require('passport-local');
const userService = require('../services/userService');


passport.use(
  'login',
  new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
    try {
        const user =  userService.search({email});
        if (!user) {
        return done({ status: 401, message: 'Incorrect email.' }, false);
      }

      return password === user.password
        ? done(null, user)
        : done({ status: 401, message: 'Passwords do not match.' }, null, false);
    } catch (err) {
      return done(err);
    }
  })
);
