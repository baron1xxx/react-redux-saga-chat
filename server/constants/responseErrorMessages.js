module.exports = {
    USER_EXITS: 'This user already exists',
    USER_NOT_CREATED: 'User not created',
    USER_NOT_FOUND: 'User not found',
    USER_NOT_UPDATED: 'User not updated',
    USER_NOT_DELETED: 'User not deleted',
    USERS_NOT_FOUND: 'Users not found',
    EMAIL_EXITS: 'This email already exists'
};
