const ErrorHandler = require('./../error/ErrorHandler');
const { OK } = require('./../constants/responseStatusCodes');

const responseMiddleware= (req, res, next) => {
    try {
        const { err, data } = req;
        return err
            ? next(new ErrorHandler(err.status, err.message))
            : res.status(OK).json(data);
    } catch (e) {
        next(new ErrorHandler(e.status, e.message));
    }
};

exports.responseMiddleware = responseMiddleware;
