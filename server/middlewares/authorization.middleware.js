const ErrorHandlerUnauthorized = require('./../error/ErrorHandlerUnauthorized');
const ErrorHandler = require('./../error/ErrorHandler');
const userService = require('../services/userService');

const authorizationMiddleware = (req, res, next) => {
  try {
    const authData = req.get('Authorization');

    if(!authData) {
      next(new ErrorHandlerUnauthorized('Not auth data.'));
    }

    const {email} = JSON.parse(authData);

    const user =  userService.search({email});

    if(!user) {
      next(new ErrorHandlerUnauthorized('Incorrect email.'));
    }

    req.user = user;
    next()
  } catch (e) {
    next(new ErrorHandler(e.status, e.message));
  }
};

exports.authorizationMiddleware = authorizationMiddleware;
