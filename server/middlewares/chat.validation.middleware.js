const ErrorHandler = require('../error/ErrorHandler');
const ErrorHandlerBadRequest = require('./../error/ErrorHandlerBadRequest');
const {chatCreateValidator, chatUpdateValidator} = require('../validators/chatValidator');

const createChatValid = (req, res, next) => {
    try {
        const {error, value: userData} = chatCreateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandlerBadRequest(` ${error.details[0].path[0]} ${error.details[0].message}`));
        }
        req.body = userData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.status, e.message));
    }

};

const updateChatValid = (req, res, next) => {
    try {
        const {error, value: userData} = chatUpdateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandlerBadRequest(` ${error.details[0].path[0]} ${error.details[0].message}`));
        }
        req.body = userData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.status, e.message));
    }
};

exports.createChatValid = createChatValid;
exports.updateChatValid = updateChatValid;
