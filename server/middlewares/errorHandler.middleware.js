const {SERVER_ERROR} = require('../constants/responseStatusCodes');

module.exports = (err, req, res, next) => {
        const { status = SERVER_ERROR, message = '' } = err;
        res.status(status).json({ error: true, message });
};
