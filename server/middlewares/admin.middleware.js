const ErrorHandlerForbidden = require('./../error/ErrorHandlerForbidden');
const ErrorHandler = require('./../error/ErrorHandler');

const adminMiddleware = (req, res, next) => {
  try {
    const { user } = req;
    return user.role === 'admin'
      ? next()
      : next(new ErrorHandlerForbidden('Only ADMIN '));
  } catch (e) {
    next(new ErrorHandler(e.status, e.message));
  }
};

exports.adminMiddleware = adminMiddleware;
