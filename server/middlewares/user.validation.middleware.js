const ErrorHandler = require('../error/ErrorHandler');
const ErrorHandlerBadRequest = require('./../error/ErrorHandlerBadRequest');
const {userCreateValidator, userUpdateValidator} = require('../validators/userValidator');

const createUserValid = (req, res, next) => {
    try {
        const {error, value: userData} = userCreateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandlerBadRequest(` ${error.details[0].path[0]} ${error.details[0].message}`));
        }
        req.body = userData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.status, e.message));
    }

};

const updateUserValid = (req, res, next) => {
    try {
        const {error, value: userData} = userUpdateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandlerBadRequest(` ${error.details[0].path[0]} ${error.details[0].message}`));
        }
        req.body = userData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.status, e.message));
    }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
