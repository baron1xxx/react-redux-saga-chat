const passport = require('passport');

exports.authenticationMiddleware = passport.authenticate('login', { session: false });
