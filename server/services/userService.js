const {UserRepository} = require('../repositories/userRepository');
const ErrorHandlerNotFound = require('./../error/ErrorHandlerNotFound');
const ErrorHandlerBadRequest = require('./../error/ErrorHandlerBadRequest');
const {
    EMAIL_EXITS,
    USER_NOT_FOUND,
    USER_NOT_CREATED,
    USER_NOT_UPDATED,
    USER_NOT_DELETED,
    USERS_NOT_FOUND,
} = require('./../constants/responseErrorMessages');

class UserService {
    getAll() {
        const users = UserRepository.getAll();
        if (Array.isArray(users) && !users.length) {
            throw new ErrorHandlerNotFound(USERS_NOT_FOUND);
        }
        return users;
    }

    getById(id) {
        const user = this.search({id});
        if (!user) {
            throw new ErrorHandlerNotFound(USER_NOT_FOUND);
        }
        return user;
    }


    create(data) {
        const {email} = data;
        this.checkIfExistsByEmail(email);
        const user = UserRepository.create(data);
        if (!user) {
            throw new ErrorHandlerBadRequest(USER_NOT_CREATED);
        }
        console.log(JSON.stringify(user));
        return user;
    }

    update(id, data) {
        const userExist = this.getById(id);
        const {email} = data;
        if (email && email !== userExist.email) {
            this.checkIfExistsByEmail(email);
        }
        const user = UserRepository.update(id, data);
        if (!user || !Object.keys(user).length) {
            throw new ErrorHandlerBadRequest(USER_NOT_UPDATED);
        }
        return user;
    }

    delete(id) {
        this.getById(id);
        const user = UserRepository.delete(id);
        if (Array.isArray(user) && !user.length) {
            throw new ErrorHandlerBadRequest(USER_NOT_DELETED);
        }
        return user;
    }

    search(search) {
        const user = UserRepository.getOne(search);
        if (!user) {
            return null;
        }
        return user;
    }

    loadUser(email) {
        const user = UserRepository.getOne({email});
        if (!user) {
            throw new ErrorHandlerNotFound(USERS_NOT_FOUND);
        }
        return user;
    }

    checkIfExistsByEmail(email) {
        const emailExists = this.search({email});
        if (emailExists) {
            throw new ErrorHandlerBadRequest(EMAIL_EXITS);
        }
    }
}

module.exports = new UserService();
