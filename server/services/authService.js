const UserService = require('./userService');
const ErrorHandlerUnauthorized = require('./../error/ErrorHandlerUnauthorized');
const {USER_NOT_FOUND} = require('./../constants/responseErrorMessages');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if (!user) {
            throw new ErrorHandlerUnauthorized(USER_NOT_FOUND);
        }
        return user;
    }
}

module.exports = new AuthService();
