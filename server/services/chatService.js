const {ChatRepository} = require('../repositories/chatRepository');
const ErrorHandlerNotFound = require('./../error/ErrorHandlerNotFound');
const ErrorHandlerBadRequest = require('./../error/ErrorHandlerBadRequest');

class ChatService {
    getAll() {
        const messages = ChatRepository.getAll();
        if (Array.isArray(messages) && !messages.length) {
            throw new ErrorHandlerNotFound('Messages not found');
        }
        return messages;
    }

    getById(id) {
        const message = this.search({id});
        if (!message) {
            throw new ErrorHandlerNotFound('Message not found');
        }
        return message;
    }


    create(data, user) {
        const {id: userId, name, avatar} = user;
        const message = ChatRepository.create({
            ...data,
            userId,
            user: name,
            avatar
        });
        if (!message) {
            throw new ErrorHandlerBadRequest('Message not created');
        }
        return message;
    }

    update(id, data) {
        this.getById(id);
        const message = ChatRepository.update(id, data);
        if (!message || !Object.keys(message).length) {
            throw new ErrorHandlerBadRequest('Message not updated');
        }
        return message;
    }

    delete(id) {
        this.getById(id);
        const message = ChatRepository.delete(id);
        if (Array.isArray(message) && !message.length) {
            throw new ErrorHandlerBadRequest('Message not deleted');
        }
        return message;
    }

    search(search) {
        const message = ChatRepository.getOne(search);
        if (!message) {
            return null;
        }
        return message;
    }
}

module.exports = new ChatService();
