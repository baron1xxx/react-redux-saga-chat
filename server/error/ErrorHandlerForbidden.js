const ErrorHandler = require('./ErrorHandler');

module.exports = class ErrorHandlerForbidden extends ErrorHandler {
  constructor(message) {
    super();
    this.status = 403;
    this.message = message;
  }
}
