const ErrorHandler = require('./ErrorHandler');

module.exports = class ErrorHandlerNotFound extends ErrorHandler {
  constructor(message) {
    super();
    this.status = 404;
    this.message = message;
  }
}
