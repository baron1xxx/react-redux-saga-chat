const ErrorHandler = require('./ErrorHandler');

module.exports = class ErrorHandlerBadRequest extends ErrorHandler {
  constructor(message) {
    super();
    this.status = 400;
    this.message = message;
  }
};
