const ErrorHandler = require('./ErrorHandler');

module.exports = class ErrorHandlerUnauthorized extends ErrorHandler {
  constructor(message) {
    super();
    this.status = 401;
    this.message = message;
  }
}
