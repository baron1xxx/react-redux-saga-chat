const Joi = require('joi');

const userValidator = Joi.object({
    name: Joi
        .string()
        .trim()
        .required()
        .min(3)
        .max(20)
        .messages({
            'string.base': 'Name must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.required': 'Name is required'
        }),
    email: Joi
        .string()
        .trim()
        .required()
        .messages({
            'string.base': 'Email must be string',
            'string.required': 'Email is required'
        }),
    password: Joi
        .string()
        .required()
        .min(3)
        .messages({
            'string.base': 'Password must be string',
            'string.required': 'Password is required',
            'string.min': 'Minimum {#limit} symbol '
        }),
    avatar: Joi
        .string()
        .trim()
        .allow('', null)
        .empty(['', null])
        .default('https://i.pinimg.com/originals/6a/87/00/6a87006a2e50cdacfd77020b8ad30868.png'),
    role: Joi
        .string()
        .trim()
        .required()
        .valid('admin', 'user')
        .messages({
            'string.required': 'Role is required'
        })
});

const userUpdateValidator = Joi.object({
    name: Joi
        .string()
        .trim()
        .min(3)
        .max(20)
        .messages({
            'string.base': 'Name must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol '
        }),
    email: Joi
        .string()
        .trim()
        .messages({
            'string.base': 'Email must be string'
        }),
    password: Joi
        .string()
        .min(3)
        .messages({
            'string.base': 'Password must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.pattern.base': 'Password doesn\'t match the pattern'
        }),
    avatar: Joi
        .string()
        .trim(),
    role: Joi
        .string()
        .trim()
        .valid('admin', 'user')
});

exports.userCreateValidator = userValidator;
exports.userUpdateValidator = userUpdateValidator;

