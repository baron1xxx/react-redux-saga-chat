const Joi = require('joi');

const chatCreateValidator = Joi.object({
    text: Joi
        .string()
        .trim()
        .required()
        .min(1)
        .messages({
            'string.base': 'Text must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.required': 'Text is required'
        })
});

const chatUpdateValidator = Joi.object({
    text: Joi
        .string()
        .trim()
        .required()
        .min(1)
        .messages({
            'string.base': 'Text must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.required': 'Text is required'
        })
});

exports.chatCreateValidator = chatCreateValidator;
exports.chatUpdateValidator = chatUpdateValidator;
