const express = require('express');
const cors = require('cors');
const passport = require('passport');
require('./config/db');
require('./config/passportConfig');
const {responseMiddleware} = require('./middlewares/response.middleware');
const errorHandlerMiddleware = require('./middlewares/errorHandler.middleware');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('./client/build'));

app.use(responseMiddleware);
app.use(errorHandlerMiddleware);

const port = 3001;
app.listen(port, () => {
    console.log(`Server started of port ${port}...`);
});

exports.app = app;
