import React, {useEffect, useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {loadUser} from '../Profile/actions'
import {loadMessages, addMessage} from '../Chat/actions'

import ChatFooter from '../../components/ChatFooter';
import ChatHeader from '../../components/ChatHeader';
import MessagesList from '../../components/MessagesList';
import Spinner from '../../components/Spinner';

import styles from './styles.module.css';

const Chat = (props) => {
    const {
        messages,
        countMessages,
        countUsers,
        dateLastMessage,
        user: authUser,
        isLoaded,
        loadUser,
        loadMessages,
        addMessage,
        editMessage,
        deleteMessage,
        reactMessage
    } = props;

    const inputRef = useRef(null);


    useEffect(() => {
        loadMessages();
    }, [loadMessages, loadUser]);

    useEffect(() => {
        inputRef.current.focus();
    }, [messages]);

    return (
        <div className={styles.chatContainer}>
            <ChatHeader
                authUser={props.user}
                countMessages={countMessages}
                countUsers={countUsers}
                dateLustMessage={dateLastMessage}
            />
            {isLoaded
                ? <MessagesList
                    messages={messages}
                    authUser={authUser}
                    reactMessage={reactMessage}
                />
                : <Spinner/>}
            <ChatFooter
                addMessage={addMessage}
                authUser={authUser}
                inputRef={inputRef}/>
        </div>
    );
};

Chat.propTypes = {
    messages: PropTypes.array,
    countMessages: PropTypes.number,
    countUsers: PropTypes.number,
    dateLastMessage: PropTypes.string,
    user: PropTypes.object,
    isLoaded: PropTypes.bool,
    loadUser: PropTypes.func,
    loadMessages: PropTypes.func,
    addMessage: PropTypes.func,
    editMessage: PropTypes.func,
    deleteMessage: PropTypes.func,
    reactMessage: PropTypes.func
};

const actions = {
    loadUser,
    loadMessages,
    addMessage
};

const mapStateToProps = rootState => ({
    messages: rootState.chat.messages,
    countMessages: rootState.chat.countMessages,
    countUsers: rootState.chat.countUsers,
    dateLastMessage: rootState.chat.dateLastMessage,
    isLoaded: rootState.chat.isLoaded,
    user: rootState.profile.user
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
