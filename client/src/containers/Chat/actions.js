import {
    LOAD_MESSAGES,
    SET_MESSAGES,
    ADD_MESSAGE,
    SET_NEW_MESSAGE,
    EDIT_MESSAGE
} from './actionTypes'

export const loadMessages = () => ({
    type: LOAD_MESSAGES
});

export const setMessages = messages => ({
    type: SET_MESSAGES,
    data: messages
});

export const addMessage = (data) => ({
    type: ADD_MESSAGE,
    data
});

export const setNewMessage = message => ({
    type: SET_NEW_MESSAGE,
    data: message
});

export const editMessage = (data) => ({
    type: EDIT_MESSAGE,
    data
});
