import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import AddUserForm from '../../components/AddUserForm';
import {addUser, editUser} from "../../containers/Users/actions";

import styles from "./styles.module.css";

const AddUserPage = ({users, error, addUser, editUser}) => {

    return (
        <div className={styles.loginContainer}>
            <AddUserForm users={users} error={error} addUser={addUser} editUser={editUser}/>
        </div>
    );
};

const mapStateToProps = ({users}) => ({
    users: users.dataList,
    error: users.isError
});

const actions = {addUser, editUser};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddUserPage);
