import {LOGIN_USER, SET_USER, SET_ERROR, CLOSE_ERROR, LOAD_USER} from './actionTypes'

export const loginUser = data => ({
    type: LOGIN_USER,
    data
});

export const loadUser = () => ({
    type: LOAD_USER
});
export const setUser = user => ({
    type: SET_USER,
    data: user
});

export const setError = message => ({
    type: SET_ERROR,
    data: message
});

export const closeError = message => ({
    type: CLOSE_ERROR,
    data: message
});

