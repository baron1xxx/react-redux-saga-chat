import {SET_USER, SET_ERROR} from './actionTypes';

const initialState = {
    isAuthorized: false,
    isLoading: false,
    user: {},
    isError: ''
};

export default (state = initialState, {type, data}) => {
    switch (type) {
        case SET_USER: {
            return {
                ...state,
                user: data,
                isAuthorized: true,
                isLoading: true,
            }
        }
        case SET_ERROR: {
            return {
                ...state,
                isError: data,
                isAuthorized: false,
                isLoading: false,

            }
        }
        default: {
            return state
        }
    }
};
