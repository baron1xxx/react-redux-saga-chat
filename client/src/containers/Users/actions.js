import {LOAD_USERS, SET_USERS, ADD_USER, SET_NEW_USER, SET_ERROR, EDIT_USER, DELETE_USER} from './actionTypes'

export const loadUsers = () => ({
    type: LOAD_USERS
});

export const addUser = (data) => ({
    type: ADD_USER,
    data
});

export const editUser = (data) => ({
    type: EDIT_USER,
    data
});

export const deleteUser = (data) => ({
    type: DELETE_USER,
    data
});

export const setUsers = users => ({
    type: SET_USERS,
    data: users
});

export const setNewUser = user => ({
    type: SET_NEW_USER,
    data: user
});

export const setError = message => ({
    type: SET_ERROR,
    data: message
});
