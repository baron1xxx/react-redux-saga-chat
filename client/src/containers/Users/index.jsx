import React, {useEffect} from 'react';
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {loadUsers, deleteUser} from "../Users/actions";
import UsersList from "../../components/UsersList";
import Spinner from "../../components/Spinner";

import styles from "./styles.module.css";

const Users = ({isLoading, users, loadUsers, deleteUser}) => {
    useEffect(() => {
        loadUsers();
    }, []);
    return (
        <div className={styles.usersContainer}>
            <Link to={'/add'}>
                <div className={styles.button}>ADD USER</div>
            </Link>
            {isLoading
                ? <UsersList users={users} deleteUser={deleteUser}/>
                : <Spinner/>
            }
        </div>
    );
};

const actions = {
    loadUsers,
    deleteUser
};

const mapStateToProps = ({users}) => ({
    isLoading: users.isLoading,
    users: users.dataList
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Users);
