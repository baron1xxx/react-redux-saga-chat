import {SET_USERS, SET_NEW_USER, SET_ERROR} from './actionTypes';

const initialState = {
    isLoading: false,
    dataList: [],
    isError: ''
};

export default (state = initialState, {type, data}) => {
    switch (type) {
        case SET_USERS: {
            return {
                ...state,
                dataList: data,
                isLoading: true,
            }
        }
        case SET_NEW_USER: {
            return {
                ...state,
                dataList: [...state.dataList, data],
            }
        }
        case SET_ERROR: {
            return {
                ...state,
                isError: data,
                isLoading: false,

            }
        }
        default: {
            return state
        }
    }
};
