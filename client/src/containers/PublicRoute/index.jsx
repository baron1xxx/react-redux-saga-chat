import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import {connect} from "react-redux";

const PublicRoute = ({component: Component, isAuthorized, user, ...rest}) => {
    return (
        <Route
            {...rest}
            render={props => (isAuthorized
                ? <Redirect to={{ pathname: '/', state: { from: props.location } }} />
                : <Component {...props} />)}
        />
    );
};

const mapStateToProps = ({profile}) => ({
    isAuthorized: profile.isAuthorized,
    user: profile.user
});

export default connect(mapStateToProps)(PublicRoute);
