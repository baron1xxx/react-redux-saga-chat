import React, {useEffect} from 'react';
import {Switch, useHistory} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PublicRoute from "../PublicRoute";
import LoginPage from "../LoginPage";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Chat from "../../containers/Chat";
import AddUserPage from "../../containers/AddUserPage";
import {loadUser} from "../Profile/actions";
import PrivateRoute from "../PrivateRoute";
import Users from "../Users";
import EditMessage from "../../components/EditMessage";

const Routing = ({isAuthorized, user, loadUser}) => {
    const history = useHistory();

    useEffect(() => {
        if (!isAuthorized && localStorage.getItem('user')) {
            loadUser();
        }
        if (!isAuthorized && !localStorage.getItem('user')) history.push("/login");
        if (isAuthorized) {
            user.role === "user" ? history.push("/chat") : history.push("/users")
        }
    }, [isAuthorized, history]);
    return (
        <>
            <Header/>
            <Switch>
                <PublicRoute exact path="/login" component={LoginPage}/>
                <PrivateRoute exact path="/chat" component={Chat}/>
                <PrivateRoute exact path="/chat/:id" component={EditMessage}/>
                <PrivateRoute exact path="/users" component={Users}/>
                <PrivateRoute exact path="/users/:id" component={AddUserPage}/>
                <PrivateRoute exact path="/add" component={AddUserPage}/>
            </Switch>
            <Footer/>
        </>
    );
};

const mapStateToProps = ({profile}) => ({
    isAuthorized: profile.isAuthorized,
    user: profile.user
});

const actions = {loadUser};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Routing);
