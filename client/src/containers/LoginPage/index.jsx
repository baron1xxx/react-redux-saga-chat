import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import LoginForm from '../../components/LoginForm';
import {loginUser, closeError } from "../../containers/Profile/actions";

import styles from "./styles.module.css";

const LoginPage = ({error, loginUser, closeError}) => {

    return (
        <div className={styles.loginContainer}>
            <LoginForm error={error} loginUser={loginUser} closeError={closeError}/>
        </div>
    );
};

const mapStateToProps = ({profile}) => ({
    error: profile.isError
});

const actions = {loginUser, closeError};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
