import callWebApi from "../helpers/webApiHelper";

export const getMessages = async () => {
    const response = await callWebApi({
        endpoint: '/api/chats',
        type: 'GET'
    });
    return response.json();
};

export const createMessages = async request => {
    const response = await callWebApi({
        endpoint: '/api/chats',
        type: 'POST',
        request
    });
    return response.json();
};

export const updateMessage = async ({id, ...request}) => {
    const response = await callWebApi({
        endpoint: `/api/chats/${id}`,
        type: 'PUT',
        request
    });
    return response.json();
};
