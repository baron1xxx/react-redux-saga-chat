import callWebApi from "../helpers/webApiHelper";

export const getUsers = async () => {
    const response = await callWebApi({
        endpoint: '/api/users',
        type: 'GET'
    });
    return response.json();
};

export const createUsers = async request => {
    const response = await callWebApi({
        endpoint: '/api/users',
        type: 'POST',
        request
    });
    return response.json();
};

export const editUsers = async ({id, ...request}) => {
    const response = await callWebApi({
        endpoint: `/api/users/${id}`,
        type: 'PUT',
        request
    });
    return response.json();
};

export const deleteUsers = async request => {
    const response = await callWebApi({
        endpoint: `/api/users/${request}`,
        type: 'DELETE'
    });
    return response.json();
};
