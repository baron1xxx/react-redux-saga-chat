import React from 'react';

import styles from "./styles.module.css";

const DateLine = ({data}) => {
    return (
        <div className={styles.dateLine}>
            {data}
        </div>
    );
};

export default DateLine;
