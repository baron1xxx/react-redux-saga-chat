import React, {useState} from 'react';

import styles from "./styles.module.css";
import ErrorMessage from "../ErrorMessage";

const LoginForm = ({error, loginUser, closeError}) => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const emailChanged = data => {
        setEmail(data);
    };

    const passwordChanged = data => {
        setPassword(data);
    };


    const handleLoginClick =  ev => {
        ev.preventDefault();
        loginUser({email, password});
    };

    const handleCloseError =  () => {
        closeError('');
    };

    return (
        <>
            {error && <ErrorMessage error={error} close={handleCloseError}/>}
            <form className={styles.formContainer} onSubmit={handleLoginClick}>
                <input
                    className={styles.input}
                    placeholder="Email"
                    name='email'
                    type="text"
                    onChange={ev => emailChanged(ev.target.value)}
                />
                <input
                    className={styles.input}
                    placeholder="Password"
                    name='password'
                    type="password"
                    onChange={ev => passwordChanged(ev.target.value)}
                />
                <button className={styles.buttonSubmit} type='submit'>Login</button>
            </form>
        </>
    );
};

export default LoginForm;
