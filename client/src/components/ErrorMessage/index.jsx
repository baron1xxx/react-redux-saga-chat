import React from 'react';

import styles from "./styles.module.css";

const ErrorMessage = ({error, close}) => {
    return (
        <div className={styles.error} onClick={close}>{error}
        <div className={styles.errorIcon}>
            <i className="far fa-window-close"></i>
        </div>
        </div>
    );
};

export default ErrorMessage;
