import React from 'react';
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';
import AvatarMessage from "../AvatarMesage";

import styles from "./styles.module.css";

const Message = ({
                     id,
                     user,
                     avatar,
                     text,
                     date,
                     userId,
                     isLiked,
                     authUser,
                     reactMessage
                 }) => {

    const history = useHistory();
    const isAuthUsersMessage = !(userId === authUser.id);

    const handleReactMessage = id => {
        reactMessage(id);
    };

    const handleEditMessage = (id) => {
        history.push(`/chat/${id}`)
    };

    const handleDeleteMessage = id => {

    };

    return (
        isAuthUsersMessage
            ? (
                <div className={styles.container}>
                    <div className={isAuthUsersMessage ? styles.messageContainer : styles.myMessageContainer}>
                        <AvatarMessage src={avatar}/>
                        <div className={isAuthUsersMessage ? styles.message : styles.myMessage}>{text}</div>
                        <div className={isLiked ? styles.like : styles.dislike} onClick={() => handleReactMessage(id)}>
                            <i className="fas fa-thumbs-up"/>
                        </div>
                        <div className={styles.date}>{date}</div>
                    </div>
                </div>
            )
            : (
                <div className={styles.container}>
                    <div className={isAuthUsersMessage ? styles.messageContainer : styles.myMessageContainer}>
                        <div className={isAuthUsersMessage ? styles.date : styles.myDate}>{date}</div>
                        <div className={isAuthUsersMessage ? styles.message : styles.myMessage}>{text}</div>
                        <div className={styles.edit} onClick={() => handleEditMessage(id)}>
                            <i className="fas fa-edit"/>
                        </div>
                        <div className={styles.delete} onClick={() => handleDeleteMessage(id)}>
                            <i className="fas fa-trash-alt"/>
                        </div>
                        <AvatarMessage src={avatar}/>
                    </div>
                </div>
            )

    );
};

Message.propTypes = {
    id: PropTypes.string,
    user: PropTypes.string,
    userId: PropTypes.string,
    avatar: PropTypes.string,
    text: PropTypes.string,
    date: PropTypes.string,
    isLiked: PropTypes.bool,
    authUser: PropTypes.object,
    reactMessage: PropTypes.func,
    setEditStatusMessage: PropTypes.func,
    setDeleteStatusMessage: PropTypes.func
};

export default Message;
