import React from 'react';
import PropTypes from 'prop-types';

import styles from "./styles.module.css";
import AddMessage from "../AddMessage";
import {addMessage} from "../../containers/Chat/actions";

const ChatFooter = ({addMessage, authUser, inputRef}) => {
    return (
        <div className={styles.chatFooterContainer}>
            <AddMessage addMessage={addMessage} authUser={authUser} inputRef={inputRef}/>
        </div>
    );
};

ChatFooter.propTypes = {
    addMessage: PropTypes.func,
    authUser: PropTypes.object,
    inputRef: PropTypes.object
};

export default ChatFooter;
