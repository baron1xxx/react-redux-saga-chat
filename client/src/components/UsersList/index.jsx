import React from 'react';
import User from "../User";

import styles from "./styles.module.css";

const UsersList = ({users, deleteUser}) => {
    return (
        <div className={styles.usersContainer}>
            {users.map((user) => (
                <User
                    key={user.id}
                    user={user}
                    deleteUser={deleteUser}
                />
            ))}
        </div>
    );
};

export default UsersList;
