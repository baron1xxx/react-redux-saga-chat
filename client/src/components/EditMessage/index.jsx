import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from "redux";
import { useHistory } from "react-router-dom";
import {connect} from "react-redux";
import {useParams} from "react-router-dom";
import {editMessage} from "../../containers/Chat/actions";

import styles from "./styles.module.css";

const EditMessage = ({messages, editMessage }) => {

    const history = useHistory();

    const {id} = useParams();
    const [text, setText] = useState('');

    useEffect(() => {
        if (id) {
            const {text} = messages.find((message) => message.id === id);
            setText(text);
        }
    }, [id, messages]);

    const messageBodyChanged = (data) => {
        setText(data);
    };

    const handleEditMessage = (ev) => {
        ev.preventDefault();
        editMessage({id, text});
        history.push('/chat')
    };

    return (
            <div className={styles.addMessageContainer}>
                <form className={styles.formContainer}>
                    <button className={styles.cancelButton}>
                        <div className={styles.sendIcon}><i className="fas fa-window-close"/></div>
                    </button>
                    <textarea
                        className={styles.textMessage}
                        value={text}
                        placeholder="Type your message..."
                        onChange={ev => messageBodyChanged(ev.target.value)}
                    >
                    </textarea>
                    <button className={styles.submitButton} type='submit' disabled={!text} onClick={(ev) => handleEditMessage(ev)}>
                        <div className={styles.sendIcon}><i className="fas fa-paper-plane"/></div>
                    </button>
                </form>
            </div>
    );
};


EditMessage.propTypes = {
    editedMessage: PropTypes.object,
    editMessage: PropTypes.func.isRequired,
    setEditStatusMessage: PropTypes.func.isRequired
};

const mapStateToProps = ({chat}) => ({
    messages: chat.messages
});

const actions = {editMessage};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);
