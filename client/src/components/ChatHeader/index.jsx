import React from 'react';
import PropTypes from 'prop-types';
import AvatarChat from "../AvatarChat";

import styles from './styles.module.css';

const ChatHeader = ({authUser, countMessages, countUsers, dateLustMessage}) => {
    return (
        <div className={styles.chatHeaderContainer}>
            <AvatarChat src={authUser?.avatar} status={authUser?.isOnline}/>
            <div className={styles.chatInfo}>
                <div>My Chat</div>
                <div>{`${countMessages} participants`}</div>
                <div>{`${countUsers} messages`}</div>
            </div>
            <div className={styles.lastMessageTime}>{`Last message at ${dateLustMessage}`}</div>
        </div>

    );
};

ChatHeader.propTypes = {
    authUser:PropTypes.object,
    countMessages: PropTypes.number,
    countUsers: PropTypes.number,
    dateLustMessage: PropTypes.string
};

export default ChatHeader;
