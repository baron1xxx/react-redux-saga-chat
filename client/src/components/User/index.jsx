import React from 'react';

import styles from "./styles.module.css";
import AvatarChat from "../AvatarChat";
import {Link} from "react-router-dom";

const User = ({user, deleteUser}) => {
    const handleDeleteUser = (id) => {
        deleteUser(id)
    };

    return (
        <div className={styles.userContainer}>
            <AvatarChat src={user.avatar}/>
            <div className={styles.name}>{user.name}</div>
            <Link to={`/users/${user.id}`}><button className={styles.editButton}>Edit</button></Link>
            <button className={styles.deleteButton} onClick={() => handleDeleteUser(user.id)}>Delete</button>
        </div>

    );
};

export default User;
