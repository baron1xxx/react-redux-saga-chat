import React, {useEffect, useState} from 'react';
import {useHistory, useParams} from "react-router-dom";

import styles from "./styles.module.css";
import ErrorMessage from "../ErrorMessage";

const AddUserForm = ({users, error, addUser, editUser, match}) => {

    const history = useHistory();
    const {id} = useParams();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        if (id) {
            const {name, email, password} = users.find((user) => user.id === id);
            setName(name);
            setEmail(email);
            setPassword(password);
        }
    }, [id, users]);


    const nameChanged = data => {
        setName(data);
    };

    const emailChanged = data => {
        setEmail(data);
    };

    const passwordChanged = data => {
        setPassword(data);
    };


    const handleAddUserClick = (ev) => {
        ev.preventDefault();
        id
            ? editUser({name, email, password, id})
            : addUser({name, email, password, role: 'user'});
        //TODO handle error. Not push if error!!! How... ???
        history.push('/users');
    };

    const cancelHandleClick = () => {
        history.push('/users');
    };

    const handleCloseError = () => {
        // closeError('');
    };

    return (
        <>
            {error && <ErrorMessage error={error} close={handleCloseError}/>}
            <form className={styles.formContainer} onSubmit={handleAddUserClick}>
                <input
                    className={styles.input}
                    placeholder="Name"
                    name='name'
                    type="text"
                    value={name}
                    onChange={ev => nameChanged(ev.target.value)}
                />
                <input
                    className={styles.input}
                    placeholder="Email"
                    name='email'
                    type="text"
                    value={email}
                    onChange={ev => emailChanged(ev.target.value)}
                />
                <input
                    className={styles.input}
                    placeholder="Password"
                    name='password'
                    type="password"
                    value={password}
                    onChange={ev => passwordChanged(ev.target.value)}
                />
                <button className={styles.buttonCancel} onClick={cancelHandleClick}>Cancel</button>
                <button className={styles.buttonSubmit} type='submit'>{id ? 'EDIT' : 'ADD'}</button>
            </form>
        </>
    );
};

export default AddUserForm;
