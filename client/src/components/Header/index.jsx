import React from 'react';

import styles from "./styles.module.css";

const Header = () => {
    return (
        <header className={styles.header}>
            <div>
                <img className={styles.imgLogo} src={'/chat-logo.png'} alt="Logo"/>
            </div>
            <div className={styles.headerChatName}>Cool React chat</div>
        </header>
    );
};

export default Header;
