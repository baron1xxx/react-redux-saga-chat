import React, {useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import Message from "../Message";
import DateLine from "../DateLine";
import groupArray from "group-array";

import styles from "./styles.module.css";


const MessagesList = ({messages, authUser, reactMessage, setEditStatusMessage, setDeleteStatusMessage}) => {
    const messagesByDay = messages.map(message => (
        {
            ...message,
            createdAt: message.createdAt.split('T')[0]
        }));
    const groupedMessages = groupArray(messagesByDay, 'createdAt');
    const messagesRef = useRef(null);
    useEffect(() => {
        if (messagesRef.current) {
            messagesRef.current.scrollTo(0, 999999);
        }
    }, [messages]);
    return (
        <div ref={messagesRef} className={styles.messagesContainer}>
            {Object.keys(groupedMessages).map(day => {
                return (
                    <React.Fragment key={day}>
                        <DateLine data={day} key={day}/>
                        {groupedMessages[day].map(({id, user, userId, avatar, text, isLiked, createdAt}) => {
                            return (
                                <Message
                                    key={id}
                                    date={createdAt}
                                    id={id}
                                    user={user}
                                    userId={userId}
                                    text={text}
                                    avatar={avatar}
                                    isLiked={isLiked}
                                    authUser={authUser}
                                    setEditStatusMessage={setEditStatusMessage}
                                    reactMessage={reactMessage}
                                    setDeleteStatusMessage={setDeleteStatusMessage}
                                />
                            )
                        })
                        }
                    </React.Fragment>
                )
            })}
        </div>
    );
};

MessagesList.propTypes = {
    messages: PropTypes.array,
    authUser: PropTypes.object,
    reactMessage: PropTypes.func,
    setEditStatusMessage: PropTypes.func,
    setDeleteStatusMessage: PropTypes.func
};

export default MessagesList;
