import {put, call, takeEvery, all} from "redux-saga/effects";
import {getUsers, createUsers, editUsers, deleteUsers} from "../services/userService";

import * as actionTypes from "../containers/Users/actionTypes";
import {setUsers, setNewUser, setError } from "../containers/Users/actions";

export function* workerLoadUsers() {
    try {
        const response = yield call(getUsers);
        if(response.error) throw Error(response.message);
        yield put(setUsers(response));
    } catch (e) {
        yield put(setError(e.message));
    }
}

export function* workerAddUser({data}) {
    try {
        const response = yield call(createUsers, data);
        if(response.error) throw Error(response.message);
        yield put(setNewUser(response));
    } catch (e) {
        alert(e.message);
        yield put(setError(e.message));
    }
}

export function* workerEditUser({data}) {
    try {
        const editedResponse = yield call(editUsers, data);
        if(editedResponse.error) throw Error(editedResponse.message);
        const response = yield call(getUsers);
        if(response.error) throw Error(response.message);
        yield put(setUsers(response));
    } catch (e) {
        alert(e.message);
        yield put(setError(e.message));
    }
}

export function* workerDeleteUser({data}) {
    try {
        const deleteResponse = yield call(deleteUsers, data);
        if(deleteResponse.error) throw Error(deleteResponse.message);
        const response = yield call(getUsers);
        if(response.error) throw Error(response.message);
        yield put(setUsers(response));
    } catch (e) {
        alert(e.message);
        yield put(setError(e.message));
    }
}

export function* watchLoadUsers() {
    yield takeEvery(actionTypes.LOAD_USERS, workerLoadUsers);
}
export function* watchAddUser() {
    yield takeEvery(actionTypes.ADD_USER, workerAddUser);
}

export function* watchEditUser() {
    yield takeEvery(actionTypes.EDIT_USER, workerEditUser);
}

export function* watchDeleteUser() {
    yield takeEvery(actionTypes.DELETE_USER, workerDeleteUser);
}


export function* usersSagas() {
    yield all([
        watchLoadUsers(),
        watchAddUser(),
        watchEditUser(),
        watchDeleteUser()
    ]);
}
