import {put, call, takeEvery, all} from "redux-saga/effects";
import {getMessages, createMessages, updateMessage} from "../services/messageServise";

import * as actionTypes from "../containers/Chat/actionTypes";
import {setMessages, setNewMessage} from "../containers/Chat/actions";

export function* workerLoadMessages() {
    try {
        const response = yield call(getMessages);
        if(response.error) throw Error(response.message);
        yield put(setMessages(response));
    } catch (e) {
        alert(e.message);
    }
}

export function* workerAddMessage({data}) {
    try {
        const response = yield call(createMessages, data);
        if(response.error) throw Error(response.message);
        yield put(setNewMessage(response));
    } catch (e) {
        alert(e.message);
    }
}

export function* workerEditMessage({data}) {
    try {
        const editedResponse = yield call(updateMessage, data);
        if(editedResponse.error) throw Error(editedResponse.message);
        const response = yield call(getMessages);
        if(response.error) throw Error(response.message);
        yield put(setMessages(response));
    } catch (e) {
        alert(e.message);
    }
}

export function* watchLoadMessages() {
    yield takeEvery(actionTypes.LOAD_MESSAGES, workerLoadMessages);
}

export function* watchAddMessage() {
    yield takeEvery(actionTypes.ADD_MESSAGE, workerAddMessage);
}

export function* watchEditMessage() {
    yield takeEvery(actionTypes.EDIT_MESSAGE, workerEditMessage);
}


export function* chatSagas() {
    yield all([
        watchLoadMessages(),
        watchAddMessage(),
        watchEditMessage()
    ]);
}
