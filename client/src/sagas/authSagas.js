import {put, call, takeEvery, all} from "redux-saga/effects";
import {login, getCurrentUser} from "../services/authService";

import * as actionTypes from "../containers/Profile/actionTypes";
import {setError, setUser} from "../containers/Profile/actions";

export function* workerLoginUser(data) {
    try {
        const response = yield call(login, data);
        if(response.error) throw Error(response.message);
        localStorage.setItem('user', JSON.stringify(response));
        yield put(setUser(response));
    } catch (e) {
        yield put(setError(e.message));
    }
}

export function* workerLoadUser() {
    try {
        const response = yield call(getCurrentUser);
        if(response.error) throw Error(response.message)
        yield put(setUser(response));
    } catch (e) {
        yield put(setError(e.message));
    }
}

export function* workerCloseAuthError({data}) {
    yield put(setError(data));
}

export function* watchUserAuthentication() {
    yield takeEvery(actionTypes.LOGIN_USER, workerLoginUser);
}

export function* watchLoadUser() {
    yield takeEvery(actionTypes.LOAD_USER, workerLoadUser);
}

export function* watchCloseAuthError() {
    yield takeEvery(actionTypes.CLOSE_ERROR, workerCloseAuthError);
}

export function* authSagas() {
    yield all([
        watchUserAuthentication(),
        watchCloseAuthError(),
        watchLoadUser()
    ]);
}
