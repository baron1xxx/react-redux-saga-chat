import {createStore, combineReducers, applyMiddleware} from "redux";
// import thunk from 'redux-thunk';
import createSagaMiddleware from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";

import { all } from 'redux-saga/effects';
import {authSagas} from './sagas/authSagas'
import {usersSagas} from './sagas/usersSagas'
import {chatSagas} from './sagas/chatSagas'

import chatReducer from './containers/Chat/reducer'
import profileReducer from './containers/Profile/reducer'
import usersReducer from './containers/Users/reducer'

const initialState = {};

const sagaMiddleware = createSagaMiddleware();
const compose = composeWithDevTools({});

const middlewares = [
    // thunk,
    sagaMiddleware
];


const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

const reducers = {
    chat: chatReducer,
    profile: profileReducer,
    users: usersReducer
};

const rootReducer = combineReducers({
    ...reducers
});

const store = createStore(rootReducer, initialState, composedEnhancers);

function* rootSaga() {
    yield all([
        authSagas(),
        usersSagas(),
        chatSagas()
    ])
};

sagaMiddleware.run(rootSaga);

export default store;
