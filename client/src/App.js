import React, {useEffect} from "react";
import './App.css';
import Routing from "./containers/Routing";

const App = () => {
    return (
        <div className="mainContainer">
           <Routing/>
        </div>
    );
};

export default App;
