export const getUserImgLink = image => (image
  ? image.link
  : 'https://i.pinimg.com/originals/6a/87/00/6a87006a2e50cdacfd77020b8ad30868.png');
